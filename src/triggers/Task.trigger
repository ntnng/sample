trigger Task on Task (before insert, after insert, before update, after update) {
    switch on Trigger.operationType {
        when BEFORE_INSERT {
            System.debug('*** Before Insert ***');
        }
        when AFTER_INSERT {
            System.debug('*** After Insert ***');        
        }
    }
}